#ifndef SYNERGY_CLIENT_H
#define SYNERGY_CLIENT_H

#include "uSynergy.h"

class SynergyClient;

struct SynergyClientCookie {
	char* serverAddress;
	int sockfd;
	SynergyClient* client;
};

struct SynergyClientThreadCookie {
	char* serverAddress;
	SynergyClient* client;
};

class SynergyClient {

public:

	void connectToServer(char* serverAddress);

private:

	static void* thread_connectToServer(void* userData);

private:

	static uSynergyBool uSynergyConnectFunc(uSynergyCookie cookie);
	static uSynergyBool uSynergyReceiveFunc(uSynergyCookie cookie, uint8_t *buffer, int maxLength, int* outLength);
	static uSynergyBool uSynergySendFunc(uSynergyCookie cookie, const uint8_t *buffer, int length);
	static uint32_t	uSynergyGetTimeFunc();
	static void uSynergySleepFunc(uSynergyCookie cookie, int timeMs);

	static void uSynergyTraceFunc(uSynergyCookie cookie, const char *text);

	static void uSynergyScreenActiveCallback(uSynergyCookie cookie, uSynergyBool active);
	static void uSynergyMouseCallback(uSynergyCookie cookie, uint16_t x, uint16_t y, int16_t wheelX, int16_t wheelY, uSynergyBool buttonLeft, uSynergyBool buttonRight, uSynergyBool buttonMiddle);
	static void uSynergyKeyboardCallback(uSynergyCookie cookie, uint16_t key, uint16_t modifiers, uSynergyBool down, uSynergyBool repeat);

private:

};

#endif