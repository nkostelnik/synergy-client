#include "SynergyClient.h"

#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 

#include <pthread.h>

void* SynergyClient::thread_connectToServer(void* userData) {
	SynergyClientThreadCookie* threadInfo = (SynergyClientThreadCookie*)userData;

	uSynergyContext context_;

	uSynergyInit(&context_);
	context_.m_clientName = "Engine";
	context_.m_clientWidth = 1024;
	context_.m_clientHeight = 768;
	
	context_.m_connectFunc = &SynergyClient::uSynergyConnectFunc;
	context_.m_receiveFunc = &SynergyClient::uSynergyReceiveFunc;
	context_.m_sendFunc = &SynergyClient::uSynergySendFunc;
	context_.m_traceFunc = &SynergyClient::uSynergyTraceFunc;
	context_.m_getTimeFunc = &SynergyClient::uSynergyGetTimeFunc;
	context_.m_sleepFunc = &SynergyClient::uSynergySleepFunc;
	context_.m_screenActiveCallback = &SynergyClient::uSynergyScreenActiveCallback;
	context_.m_mouseCallback = &SynergyClient::uSynergyMouseCallback;
	context_.m_keyboardCallback = &SynergyClient::uSynergyKeyboardCallback;

	SynergyClientCookie cookie_;
	cookie_.serverAddress = threadInfo->serverAddress;
	cookie_.sockfd = 0;
	cookie_.client = threadInfo->client;
	context_.m_cookie = (uSynergyCookie)&cookie_;

	while(true) {
		uSynergyUpdate(&context_);
	}
}

void SynergyClient::connectToServer(char* serverAddress) {
	SynergyClientThreadCookie threadInfo;
	threadInfo.serverAddress = serverAddress;

	pthread_t thread;
	int threadId = pthread_create(&thread, NULL, SynergyClient::thread_connectToServer, &threadInfo);

	if (threadId) {
		printf("Error creating thread\n");
	}
}

uSynergyBool SynergyClient::uSynergyConnectFunc(uSynergyCookie cookie) {
	SynergyClientCookie* clientCookie = (SynergyClientCookie*)cookie;

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (!sockfd) {
		printf("failed to create socket\n");
		return 1;
	}

	clientCookie->sockfd = sockfd;
	
	struct sockaddr_in serv_addr;
	serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(24800);

    if(inet_pton(AF_INET, clientCookie->serverAddress, &serv_addr.sin_addr) <= 0) {
        printf("inet_pton error occured\n");
        return false;
    } 

    if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
       printf("Error : Connect Failed \n");
       return false;
    } 

    return true;
}

uSynergyBool SynergyClient::uSynergyReceiveFunc(uSynergyCookie cookie, uint8_t *buffer, int maxLength, int* outLength) {
	SynergyClientCookie* clientCookie = (SynergyClientCookie*)cookie;
	*outLength = read(clientCookie->sockfd, buffer, maxLength);
	bool success = !(*outLength < 0);
	return success;
}

uSynergyBool SynergyClient::uSynergySendFunc(uSynergyCookie cookie, const uint8_t *buffer, int length) {
	SynergyClientCookie* clientCookie = (SynergyClientCookie*)cookie;
	int result = write(clientCookie->sockfd, buffer, length);
	bool success = result > -1;
	return success;
}

void SynergyClient::uSynergyTraceFunc(uSynergyCookie cookie, const char *text) {
	printf("%s\n", text);
}

uint32_t SynergyClient::uSynergyGetTimeFunc() {
	return 0;
}

void SynergyClient::uSynergySleepFunc(uSynergyCookie cookie, int timeMs) {
	usleep(timeMs * 1000);
}

void SynergyClient::uSynergyScreenActiveCallback(uSynergyCookie cookie, uSynergyBool active) {
	if (active) {
		printf("Screen Activated\n");
	}
	else {
		printf("Screen Deactivated\n");	
	}
}
	
void SynergyClient::uSynergyMouseCallback(uSynergyCookie cookie, uint16_t x, uint16_t y, int16_t wheelX, int16_t wheelY, uSynergyBool buttonLeft, uSynergyBool buttonRight, uSynergyBool buttonMiddle) {
	printf("Mouse x:%d y:%d\n", x, y);
}

void SynergyClient::uSynergyKeyboardCallback(uSynergyCookie cookie, uint16_t key, uint16_t modifiers, uSynergyBool down, uSynergyBool repeat) {

	printf("Keyboard code:%d\n", key);
}